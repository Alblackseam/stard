﻿using System;
using UnityEngine;


public class Orbit {
    public Vector3 rotationSpeed;
    public Vector3 center;
    public Vector3 axisA;
    public Vector3 axisB;
    public float phi;
    public float angularSpeed;

    public Orbit(Vector3 orbitCenter, Vector3 vectorA, float b) {
        center = orbitCenter;
        axisA = vectorA - orbitCenter;
        axisB = Vector3.Cross(axisA, -Vector3.up) * b;
    }

    public Vector3 position {
        get {
            return center + (axisA * Mathf.Sin(phi) + axisB * Mathf.Cos(phi));
        }
    }
}


public class Asteroid : MonoBehaviour {
    //public Asteroid(Vector3 orbitCenter, Vector3 vectorA, float b) {
    //}

    //public GameObject go = null;
    public Orbit orbit;
    private float health = 1;


    public void update(float dt) {
        if (gameObject == null)
            return;
        orbit.phi += orbit.angularSpeed * dt;
        gameObject.transform.position = orbit.position;
        gameObject.transform.Rotate(orbit.rotationSpeed.x, orbit.rotationSpeed.y, orbit.rotationSpeed.z);
    }

    internal void damage(float v) {
        health -= v;
        if (health > 0)
            return;
    
        var explosionPrefab = Resources.Load("Prefabs/Explosion", typeof(GameObject)) as GameObject;
        var explosionCreated = GameObject.Instantiate(explosionPrefab, gameObject.transform.position, gameObject.transform.rotation);
        GameObject.Destroy(gameObject, 0.1f);
        GameObject.Destroy(explosionCreated, 1);
    }






    //private void OnCollisionEnter(Collision collision) {
    //public void handleCollision() {
    //    var explosionPrefab = AssetDatabase.LoadAssetAtPath(@"Assets/Prefabs/Explosion.prefab", typeof(GameObject)) as GameObject; //    explosionPrefab.transform.position = new Vector3(1000, 10000, 0);
    //    var explosionCreated = Instantiate(explosionPrefab, transform.position, transform.rotation);
    //    Destroy(gameObject, 1);
    //    Destroy(explosionCreated, 1);
    //}
}


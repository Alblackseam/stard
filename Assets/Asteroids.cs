﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;



public class Asteroids : MonoBehaviour {
    int r = 79;
    const int MaxRandomValue = 2147483647;
    public int nextPseudoRandomValue() {
        const int p1 = 16807;
        const int p2 = 0;
        r = (p1 * r + p2) % MaxRandomValue;
        return r;
    }

    float Random(float maxValue) {
        return nextPseudoRandomValue() * maxValue / MaxRandomValue;
    }

    Asteroid[] asteroids = null;



    void Start() {
        var asteroidPrefabs = new GameObject[5];
        for (int i = 0; i < 5; i++) {
            //asteroidPrefabs[i] = AssetDatabase.LoadAssetAtPath(@"Assets/Asteroids pack/Prefabs/Asteroid_" + i +".prefab", typeof(GameObject)) as GameObject;
            asteroidPrefabs[i] = Resources.Load($"Prefabs/Asteroid_{i}", typeof(GameObject)) as GameObject;
        }

        const int asteroidsCount = 500;
        const float areaRadius = 3000;
        const float orbitRadiusMin = 5;
        const float orbitRadiusMax = 20;
        const float minPlanetRadius = 1000;
        const float maxPlanetRadius = 10000;

        asteroids = new Asteroid[asteroidsCount];

        for (int i = 0; i < asteroidsCount; ++i) {
            var radius = nextPseudoRandomValue() * (maxPlanetRadius - minPlanetRadius) / 2147483647 + minPlanetRadius;
            var asteroidType = (int)(Mathf.Abs(nextPseudoRandomValue()) * 5.0f / 2147483647);
            var maxRotSpeed = 0.3f;

            //if (asteroidType >= 5) {
            //    Debug.Log($"asteroidType `{asteroidType}` >= 5 !!!");
            //    asteroidType = 4;
            //}

            var orbitCenter = new Vector3(Random(areaRadius), 0, Random(areaRadius));
            var vectorA = orbitCenter + new Vector3(orbitRadiusMin + Random(orbitRadiusMax - orbitRadiusMin), 0, orbitRadiusMin + Random(orbitRadiusMax - orbitRadiusMin));
            var b = Random(orbitRadiusMax - orbitRadiusMin);


            var orbit = new Orbit(orbitCenter, vectorA, b) {
                phi = Random(2 * Mathf.PI),
                rotationSpeed = new Vector3(Random(maxRotSpeed), Random(maxRotSpeed), Random(maxRotSpeed)),
                angularSpeed = 0.3f + Random(0.7f)
            };

            var asteroidGo = Instantiate(asteroidPrefabs[asteroidType], orbit.position, Quaternion.identity, transform);
            asteroidGo.transform.localScale = new Vector3(radius, radius, radius);
            var asteroid = asteroidGo.AddComponent<Asteroid>();
            asteroids[i] = asteroid;
            asteroid.orbit = orbit;
        }
    }


    void Update() {
        if (!SceneInstance.instance.playing)
            return;
        foreach (var a in asteroids) {
            if (a)
                a.update(Time.deltaTime);
        }
    }
}

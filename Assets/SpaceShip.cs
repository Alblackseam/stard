﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


[RequireComponent(typeof(AudioSource))]
public class SpaceShip : MonoBehaviour {

    public float speed = 1.0f;
    public float maxSpeed = 30.0f;
    public float acceleration = 10;
    public float rotationSpeed = 100.0f;
    public float projectileSpeed = 100;

    public AudioClip otherClip;
    public Material materialVertices;
    private Mesh mesh;

    public Camera camera;
    private Rigidbody projectile;
    public float health {
        get;
        private set;
    } = 1.0f;




    void Start() {
        projectile = Resources.Load("Prefabs/Projectile", typeof(Rigidbody)) as Rigidbody; //projectile = AssetDatabase.LoadAssetAtPath(@"Assets/Prefabs/Projectile.prefab", typeof(Rigidbody)) as Rigidbody;
        projectile.transform.position = new Vector3(0,10000,0);

        //materialVertices = new Material(Shader.Find("Standard"));
        mesh = new Mesh();
        var near = camera.nearClipPlane + 0.01f;
        var width = 0.5f * near;
        var height = 0.5f * near;

        Vector3[] vertices = new Vector3[4]
        {
            new Vector3(0, 0, near),
            new Vector3(width, 0, near),
            new Vector3(0, height, near),
            new Vector3(width, height, near)
        };
        mesh.vertices = vertices;

        int[] tris = new int[6]
        {
            // lower left triangle
            0, 2, 1,
            // upper right triangle
            2, 3, 1
        };
        mesh.triangles = tris;

        Vector3[] normals = new Vector3[4]
        {
            -Vector3.forward,
            -Vector3.forward,
            -Vector3.forward,
            -Vector3.forward
        };
        mesh.normals = normals;

        Vector2[] uv = new Vector2[4]
        {
            new Vector2(0, 0),
            new Vector2(1, 0),
            new Vector2(0, 1),
            new Vector2(1, 1)
        };
        mesh.uv = uv;


        //var aSource = GetComponent<AudioSource>();
        //aSource.clip = otherClip;
        //aSource.PlayScheduled(0.5);
    }


    void Update() {
        if (!SceneInstance.instance.playing)
            return;

        speed += Input.GetKey(KeyCode.UpArrow) ? acceleration * Time.deltaTime : 0;
        speed -= Input.GetKey(KeyCode.DownArrow) ? acceleration * Time.deltaTime : 0;
        speed = Mathf.Clamp(speed, 0, maxSpeed);

        float rotation = 0;
        rotation += Input.GetKey(KeyCode.RightArrow) ? speed / maxSpeed : 0;
        rotation -= Input.GetKey(KeyCode.LeftArrow) ? speed / maxSpeed : 0;

        if (Input.GetKeyDown(KeyCode.Space)) {
            var p = Instantiate(projectile, transform.position + transform.forward * 6, transform.rotation);
            p.velocity = transform.forward * projectileSpeed;
            Destroy(p.gameObject, 10);
        }

        transform.Translate(0, 0, speed * Time.deltaTime);
        transform.Rotate(0, rotation, 0);

        var angle = Mathf.PI * (0.1f * speed / maxSpeed + 0.4f);
        var viewDirection = transform.forward * Mathf.Sin(angle) - Vector3.up * Mathf.Cos(angle);
        camera.transform.position = transform.position + Vector3.up * 3 + viewDirection * (-15);
        camera.transform.rotation = Quaternion.LookRotation(viewDirection, -Vector3.Cross(transform.right, viewDirection));



        //var testPos = new Rect(0,0,500,200);
        //Vector3[] vertices = new Vector3[4];
        //vertices[0] = new Vector3(0, 0, camera.nearClipPlane);
        //vertices[1] = new Vector3(500, 0, camera.nearClipPlane);
        //vertices[2] = new Vector3(0, 100, camera.nearClipPlane);
        //vertices[3] = new Vector3(500, 100, camera.nearClipPlane);
        //for (int i = 0; i < 4; i++) {
        //    //vertices[i] = camera.ViewportToWorldPoint(vertices[i]);
        //}
        //mesh.vertices = vertices;


        //materialVertices.SetPass(0);
        //Graphics.DrawMesh(mesh, camera.transform.position, camera.transform.rotation, materialVertices, 0);
    }


    private void OnTriggerEnter(Collider other) {
        if (health <= 0)
            return;

        //Debug.Log($"{other.gameObject}");
        health -= 0.1f;

        if (health <= 0) {
            Debug.Log($"health: {health}");
            SceneInstance.instance.shipCrash();
        }
    }
}

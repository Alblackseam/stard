﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public static class SceneInstance {
    static public GameScene instance = null;
}

public class GameScene : MonoBehaviour {

    public enum State { Paused, Playing, Destroyed }
    public State state { get; private set; }

    public bool playing {
        get { return state == State.Playing; }
    }


    //private Asteroids asteroids = new Asteroids();


    void Start() {
        SceneInstance.instance = this;

        state = State.Playing;
    }

    void OnGUI() {
        var aSource = GetComponent<AudioSource>();

        if (state == State.Playing) {
            if (GUI.Button(new Rect(400, 0, 150, 30), "Pause")) {
                aSource.Pause();
                Debug.Log("Pause: " + aSource.time);
                state = State.Paused;
                Time.timeScale = 0;
            }
        }

        if (state == State.Paused) {
            if (GUI.Button(new Rect(400, 70, 150, 30), "Continue")) {
                aSource.UnPause();
                state = State.Playing;
                Time.timeScale = 1;
            }
        }

        if (state == State.Destroyed) {
            if (GUI.Button(new Rect(400, 70, 150, 30), "Restart")) {
                state = State.Playing;
                Time.timeScale = 1;
				SceneManager.LoadScene("Main", LoadSceneMode.Single);
            }
        }
    }

    internal void playSound(string soundName) {
        var aSource = GetComponent<AudioSource>();
        aSource.clip = Resources.Load(soundName, typeof(AudioClip)) as AudioClip;
        aSource.Play();
    }

    internal void shipCrash() {
        playSound("ShipCrash");
        state = State.Destroyed;
        Time.timeScale = 0;
    }
}






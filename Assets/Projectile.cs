﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;




public class Projectile : MonoBehaviour {
    private void OnTriggerEnter(Collider other) {
        var asteroid = other.gameObject.GetComponent<Asteroid>();
        if (asteroid != null) {
            asteroid.damage(0.3f);
        }
        Destroy(gameObject);
    }
}

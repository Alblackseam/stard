using System;
using UnityEngine;
using System.Linq;



struct TextureTools {

    public static void readPixelsToTexture(RenderTexture sourceRenderTexture, Texture2D texture) {
        int renderTargetWidth = sourceRenderTexture.width;
        int renderTargetHeight = sourceRenderTexture.height;
        Rect rectReadPicture = new Rect(0, 0, renderTargetWidth, renderTargetHeight);
        RenderTexture.active = sourceRenderTexture;
        texture.ReadPixels(rectReadPicture, 0, 0);
        texture.Apply();
        RenderTexture.active = null; // added to avoid errors 
    }

    static public Texture2D CreateTexture2dByRT(RenderTexture renderTexture) {
        TextureFormat resultTextureFormat;
        switch (renderTexture.format) {
            case RenderTextureFormat.RFloat:
                resultTextureFormat = TextureFormat.RFloat;
                break;
            case RenderTextureFormat.ARGBFloat:
                resultTextureFormat = TextureFormat.RGBAFloat;
                break;
            default:
                throw new Exception("Unsupported format");
        }

        var resultTexture = new Texture2D(renderTexture.width, renderTexture.height, resultTextureFormat, false) {
            filterMode = FilterMode.Point
        };
        resultTexture.Apply(updateMipmaps: false);
        readPixelsToTexture(renderTexture, resultTexture);
        return resultTexture;
    }

    internal static float GetIntensity(RenderTexture mappedPosesRenderTextureKeys, int x, int y, int w, int h) {
        float result = 0;
        var data = TextureTools.CreateTexture2dByRT(mappedPosesRenderTextureKeys).GetRawTextureData<float>();
        for (int row = y; row < y + h; ++row) {
            for (int col = x; col < x + w; ++col) {
                result += data[row * mappedPosesRenderTextureKeys.width + col];
            }
        }
        return result;
    }

    static public Texture2D[] CreateTextures2dByRT(RenderTexture[] renderTextures) {
        var resultTextures = new Texture2D[renderTextures.Length];

        for (int i = 0; i < renderTextures.Length; ++i) {
            resultTextures[i] = new Texture2D(renderTextures[i].width, renderTextures[i].height, TextureFormat.RGBAFloat, false) {
                filterMode = FilterMode.Point
            };
            resultTextures[i].Apply(updateMipmaps: false);
            readPixelsToTexture(renderTextures[i], resultTextures[i]);
        }
        return resultTextures;
    }

    internal static void NormalizeIntensity(Texture2D texture) {
        var textureData = texture.GetRawTextureData<float>();
        double matrixMin = double.MaxValue;
        double matrixMax = double.MinValue;

        for (int y = 0; y < texture.height; ++y) {
            for (int x = 0; x < texture.width; ++x) {
                var value = textureData[y * texture.width + x];
                if (value > matrixMax)
                    matrixMax = value;
                if (value < matrixMin)
                    matrixMin = value;
            }
        }

        for (int y = 0; y < texture.height; ++y) {
            for (int x = 0; x < texture.width; ++x) {
                var value = textureData[y * texture.width + x];
                textureData[y * texture.width + x] = (float)((value - matrixMin) / (matrixMax - matrixMin));
            }
        }
        texture.SetPixelData<float>(textureData, 0);
        texture.Apply();
    }
}
